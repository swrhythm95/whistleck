//
//  MyGenresViewController.swift
//  whistleCK
//
//  Created by Steven Wijaya on 17/05/20.
//  Copyright © 2020 Steven Wijaya. All rights reserved.
//

import UIKit
import CloudKit

class MyGenresViewController: UITableViewController {

	var myGenres: [String]!
    override func viewDidLoad() {
        super.viewDidLoad()

		let defaults = UserDefaults.standard
		if let savedGenres = defaults.object(forKey: "myGenres") as? [String] {
			myGenres = savedGenres
		} else {
			myGenres = [String]()
		}
		
		title = "Notify me about…"
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return SelectGenreViewController.genres.count
    }

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		
		let genre = SelectGenreViewController.genres[indexPath.row]
		cell.textLabel?.text = genre
		
		if myGenres.contains(genre) {
			cell.accessoryType = .checkmark
		} else {
			cell.accessoryType = .none
		}
		
		return cell
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let cell = tableView.cellForRow(at: indexPath) {
			let selectedGenre = SelectGenreViewController.genres[indexPath.row]
			
			if cell.accessoryType == .none {
				cell.accessoryType = .checkmark
				myGenres.append(selectedGenre)
			} else {
				cell.accessoryType = .none
				
				if let index = myGenres.firstIndex(of: selectedGenre) {
					myGenres.remove(at: index)
				}
			}
		}
		
		tableView.deselectRow(at: indexPath, animated: false)
	}
	@objc func saveTapped() {
		print("1")
		let defaults = UserDefaults.standard
		defaults.set(myGenres, forKey: "myGenres")
		print("2")
		let database = CKContainer.default().publicCloudDatabase
		print("3")
		database.fetchAllSubscriptions { [unowned self] subscriptions, error in
			if error == nil {
				print("-1")
				if let subscriptions = subscriptions {
					print("--1")
					for subscription in subscriptions {
						print("---1 \(subscription)")
						database.delete(withSubscriptionID: subscription.subscriptionID) { str, error in
							print("----1")
							if error != nil {
								// do your error handling here!
								print("-----1")
								print(error!.localizedDescription)
							}
							print("----2")
						}
						print("---2 \(subscription)")
					}
					print("--2")
					for genre in self.myGenres {
						let predicate = NSPredicate(format:"genre = %@", genre)
						let subscription = CKQuerySubscription(recordType: "Whistles", predicate: predicate, options: .firesOnRecordCreation)
						
						let notification = CKSubscription.NotificationInfo()
						notification.alertBody = "There's a new whistle in the \(genre) genre."
						notification.soundName = "default"
						
						subscription.notificationInfo = notification
						
						database.save(subscription) { result, error in
							if let error = error {
								print(error.localizedDescription)
							}
						}
					}
					print("--3")
				}
				print("-2")
			} else {
				print("-3")
				// do your error handling here!
				print(error!.localizedDescription)
			}
		}
		print("4")
	}
}
